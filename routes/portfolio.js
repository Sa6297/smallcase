var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();
var Stock = require("../models/stock");
var Trade = require("../models/trade");

// GET portfolio
router.route("/").get(function(req, res) {
  Stock.aggregate([
    {
      $lookup: {
        from: "trades",
        localField: "_id",
        foreignField: "stock",
        as: "trades"
      }
    },
    {
      $project: {
        _id: 0,
        __v: 0,
        "trades.stock": 0,
        "trades.__v": 0
      }
    }
  ]).exec(function(err, trades) {
    if (err) res.send({ success: false, message: err });
    res.send({ success: true, data: trades });
  });
});

// GET holdings
router.route("/holdings").get(function(req, res) {
  getHoldings
    .exec()
    .then(function(holdings) {
      res.send({ success: true, data: holdings });
    })
    .catch(function(err) {
      if (err) res.send({ success: false, message: err });
    });
});

// GET Returns
router.route("/returns").get(function(req, res) {
  getHoldings
    .exec()
    .then(function(holdings) {
      for (key in holdings) {
        holdings[key].cumulativeReturn =
          (100 - holdings[key].averageBuyPrice) * holdings[key].holdingQuantity;
      }
      res.send({ success: true, data: holdings });
    })
    .catch(function(err) {
      if (err) res.send({ success: false, message: err });
    });
});

// Add a Trade
router.route("/addTrade").post(function(req, res) {
  Stock.findOne({ tradeName: req.body.tradeName }, function(err, stock) {
    if (err) res.send({ success: false, message: err });
    let tradeType = req.body.type.toUpperCase();
    if (["BUY", "SELL"].indexOf(tradeType) == -1) {
      res.send({ success: false, message: "Bad Request" });
    }
    var trade = new Trade({
      type: tradeType,
      quantity: req.body.quantity,
      price: req.body.price,
      date: new Date(),
      stock: stock._id
    });
    trade.save(function(err, trade) {
      if (err) res.send({ success: false, message: err });
      res.send({ success: true, data: trade });
    });
  });
});

//Delete a Trade
router.route("/deleteTrade/:id").post(function(req, res) {
  Trade.findByIdAndRemove(req.params.id, function(err, trade) {
    if (err) res.send({ success: false, message: err });
    res.send({ success: true, message: "Deleted" });
  });
});

//Update a Trade
router.route("/updateTrade/:id").post(function(req, res) {
  Trade.findById(req.params.id, function(err, trade) {
    if (err) res.send(utils.getError(err));
    trade.type = req.body.trade_type;
    trade.quantity = req.body.quantity;
    trade.price = req.body.price;
    trade.save(function(err, trade) {
      if (err) res.send(utils.getError(err));
      else res.send({ success: true, data: trade });
    });
  });
});

//function to obtain holdings
var getHoldings = Trade.aggregate([
  {
    $project: {
      quantity: 1,
      price: 1,
      stock: 1,
      type: 1,
      buyQuantity: {
        $cond: { if: { $eq: ["$type", "BUY"] }, then: "$quantity", else: 0 }
      },
      holdingQuantity: {
        $cond: {
          if: { $eq: ["$type", "BUY"] },
          then: "$quantity",
          else: { $subtract: [0, "$quantity"] }
        }
      }
    }
  },
  {
    $group: {
      _id: { stock: "$stock" },
      totalBuyQuantity: { $sum: "$buyQuantity" },
      totalBuyAmount: { $sum: { $multiply: ["$price", "$buyQuantity"] } },
      holdingQuantity: { $sum: "$holdingQuantity" }
    }
  },
  {
    $project: {
      holdingQuantity: 1,
      averageBuyPrice: { $divide: ["$totalBuyAmount", "$totalBuyQuantity"] }
    }
  },
  {
    $lookup: {
      from: "stocks",
      localField: "_id.stock",
      foreignField: "_id",
      as: "stock"
    }
  },
  {
    $project: {
      _id: 0,
      "stock.__v": 0
    }
  }
]);

module.exports = router;
