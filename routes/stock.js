var express = require("express");
var mongoose = require("mongoose");
var router = express.Router();
var Stock = require("../models/stock");

// Add a Stock
router.route("/").post(function(req, res) {
  var stock = new Stock({
    tradeName: req.body.tradeName,
    name: req.body.name
  });

  stock.save(function(err, stock) {
    if (err) res.send({ success: false, message: err });
    res.send({ success: true, data: stock });
  });
});

// Retrive all Stocks
router.route("/").get(function(req, res) {
  Stock.find(function(err, stock) {
    if (err) res.send({ success: false, message: err });
    res.send({ success: true, data: stock });
  });
});

module.exports = router;
