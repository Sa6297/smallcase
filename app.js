var express = require("express");
var dotenv = require("dotenv");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var app = express();
dotenv.load();
mongoose.connect(process.env.MONGO_DB_URI);

var portfolioRoutes = require("./routes/portfolio");
var stockRoutes = require("./routes/stock");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/portfolio", portfolioRoutes);
app.use("/stock", stockRoutes);
app.listen(3000);
module.exports = app;
