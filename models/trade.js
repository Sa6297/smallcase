var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TradeSchema = new Schema({
  type: String,
  quantity: Number,
  price: Number,
  date: Date,
  stock: { type: mongoose.Schema.Types.ObjectId, ref: "Stock" }
});

module.exports = mongoose.model("Trade", TradeSchema);
