var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var StockSchema = new Schema({
  tradeName: { type: String, required: true },
  name: { type: String, required: true }
});

module.exports = mongoose.model("Stock", StockSchema);
