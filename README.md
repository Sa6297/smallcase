API Endpoints:

#GET request for obtaining stockList
GET http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/stock

#POST request for obtaining adding stocks
POST http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/stock

Request Format:
{
	name: "String",
	tradeName: "String"	
}

#GET request for obtaining portfolio 
GET http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/portfolio

#GET request for obtaining holdings 
GET http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/portfolio/holdings

#GET request for obtaining cumulative returns 
GET http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/portfolio/returns

#POST request for adding trades
POST http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/portfolio/addtrade

Request Format:
{
	type:["BUY","SELL:],
	tradeName: "String",
	quantity: Number,
	price: Number
}

#POST request for updating Trades
POST http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/portfolio/updatetrade/:id

Request Format:
{
	type:["BUY","SELL:],
	quantity: Number,
	price: Number
}

#POST request for deleting Trades
POST http://ec2-54-185-181-126.us-west-2.compute.amazonaws.com:3000/portfolio/deletetrade/:id

Design Decisions:

-> I have used ObjectId referencing by storing parent's objectId into the child Object for association to reduce the size of the objects and to avoid Maximum Document Size reached error in case of a large number of trades. This makes the design scalable.

-> I have preferred to use pipelining/lookup while querying the data from the database as it returns JSON data instead of Mongo Document which results in a better response time. And also the calculations, filtering, and the selection of data happens in the Mongo Environment itself, resulting in smaller overheads when response from database is sent to the node application.

-> I have modularised the code and reused the existing code wherever possible.

-> There definitely are scopes for improvement, but I think it covers up all the functionalities mentioned in the task in an efficient manner.